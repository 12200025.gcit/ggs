<?php

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    $products = Product::all(); 

    // Call the showCartItems function from CartController
    $cartData = app(CartController::class)->showCartItems();

    return view('home', [
        'products' => $products,
        'cartItems' => $cartData['cartItems'] ?? null,
        'totalPrice' => $cartData['totalPrice'] ?? null,
    ]);
})->name('home');

Route::get('/login', function() {
    return view('login');
});

Route::get('/register', function() {
    return view('register');
});



Route::namespace('App\Http\Controllers')->group(function () {
    // user

    Route::post('/login', 'LoginController@login')->name('login');
    Route::post('/register', 'LoginController@register')->name('register');
    Route::post('/logout', 'LoginController@logout')->name('logout');
});


Route::middleware(['auth'])->group(function () {
    Route::get('/shopHome', function () {
        $products = Product::all(); 
    
        // Call the showCartItems function from CartController
        $cartData = app(CartController::class)->showCartItems();
    
        return view('shopowner.products', [
            'products' => $products,
            'cartItems' => $cartData['cartItems'] ?? null,
            'totalPrice' => $cartData['totalPrice'] ?? null,
        ]);
    });

    Route::get('/product', function() {
        $products = Product::all(); 
    
        // Call the showCartItems function from CartController
        $cartData = app(CartController::class)->showCartItems();
    
        return view('products', [
            'products' => $products,
            'cartItems' => $cartData['cartItems'] ?? null,
            'totalPrice' => $cartData['totalPrice'] ?? null,
        ]);
    })->name('product.index');

    Route::get('/shop/users', function (){
        $users = User::where('role', 'user')->get(); // Fetch users with the role 'user'
        return view('shopowner.users', ['users' =>$users]);

    });

    Route::delete('/users/{id}', [UserController::class, 'deleteUser'])->name('users.delete');

});

Route::get('/shop/products', function () {
    $products = Product::all(); // Fetch all products

    return view('shopowner.products', ['products' => $products]);
});

Route::post('/products', [ProductController::class, 'addProduct'])->name('products.add');

Route::post('/cart/add/{productId}', [CartController::class, 'addToCart'])->name('cart.add');

Route::get('/cart/display', [CartController::class, 'showCartItems'])->name('show.cart.items');
Route::delete('/cart/remove/{productId}', [CartController::class, 'removeFromCart']);
Route::delete('/shop/remove/{id}', [ProductController::class, 'removeItems'])->middleware('auth');



Route::get('/sort', [ProductController::class, 'sorts'])->name('sortProducts');
Route::get('/search', [ProductController::class, 'searchs'])->name('searchProducts');

Route::post('/order', [OrderController::class, 'checkout'])->name('checkout');
