<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    public function checkout(Request $request)
    {
        // Implement checkout logic, create orders and order items
        // Example:
        $order = Order::create([
            'user_id' => auth()->user()->id, // Assuming you have authentication
            'total_price' => (float) str_replace(',', '', $request->input('total_price')),

        ]);

        // foreach ($request->input('items') as $item) {
        //     OrderItem::create([
        //         'order_id' => $order->id,
        //         'product_id' => $item['product_id'],
        //         'quantity' => $item['quantity'],
        //         'price' => $item['price'],
        //     ]);
        // }

        // Additional logic...
        $userId = Auth::id();

    // Delete cart items for the specific user
    Cart::truncate();
    Session::flash('success', 'Order has been placed, you may procced to the store');
    return redirect('/product');
}
}
