<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response;

class CartController extends Controller
{
    public function addToCart(Request $request, $productId)
    {
        if (!Auth::check()) {
            // If the user is not logged in, redirect to the login page

            return redirect()->route('login')->with('message', 'Please log in to add items to your cart.');
        }
        // Validate the request
        $request->validate([
            'quantity' => 'required|integer|min:1',
        ]);
        

        // Get the product
        $product = Product::findOrFail($productId);



    // Check if the requested quantity is available
    if ($request->quantity > $product->quantity) {
        // If not available, redirect back with an error message
        Session::flash('error', 'Product quantity not available.');
        return redirect()->back()->withErrors(['message' => 'Product quantity not available.']);
    }

        // Create or update the cart item
        $cart = Cart::updateOrCreate(
            ['product_id' => $product->id],
            ['quantity' => $request->quantity, 'price' => $product->price]
        );
        Session::flash('success', 'Item added to cart');

       return redirect('/product');
        
    
        
        // Redirect to showCartItems function
    }

    public function showCartItems()
    {
        // Fetch the updated cart items
        $cartItems = Cart::with('product')->get();

        // Calculate the total price
        $totalPrice = $cartItems->sum(function ($cartItem) {
            return $cartItem->quantity * $cartItem->price;
        });

        // Return the updated cart items and total price
        return [
            'cartItems' => $cartItems,
            'totalPrice' => $totalPrice,
        ];
    }

    public function removeFromCart($productId)
{
    Cart::where('product_id', $productId)->delete();
    Session::flash('success', 'Item has been removed');
    return redirect('/product');
}

}
