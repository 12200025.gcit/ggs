<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GGS</title>

    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <!-- Include Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/header.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500&display=swap">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css" />


<style>
    @media (max-width: 767px) {
        /* Adjust font size for small screens */
        .text-md {
            font-size: 1.5rem; /* Example size, adjust as needed */
        }

        .text-sm {
            font-size: 1rem; /* Example size, adjust as needed */
        }

    }
    body {
            overflow-x: hidden; /* Disable horizontal scrolling */
        }
</style>
</head>

<body>
    
@include('header')
@include('hero')
<div class="container-fluid" style="padding: 0 15px; margin-top: 50px;">
    <div class="row">
        <!-- Left Div with Image -->
        <div class="col-md-6 d-flex justify-content-center" data-aos="fade-right">
            <img src="{{ asset('images/logo.svg') }}" alt="Image" class="img-fluid">
        </div>

        <!-- Right Div with Description -->
        <div class="col-md-6" id="about" data-aos="fade-left">
            <div>
                <h2 class="text-md" data-aos="fade-up">GCIT Grocery Store</h2>
                <p class="text-sm" data-aos="fade-up" data-aos-delay="100">
                    Elevating your shopping experience at GCIT. As winter sets in and the temperature drops, 
                    we understand the frustration that comes with traditional in-store visits. 
                    Finding essential items out of stock, wandering the aisles in search of products, 
                    and managing long shopping lists in the freezing weather can be challenging.
                </p>
                <!-- Add more content as needed -->
            </div>
        </div>
    </div>
</div>


<h1 style="margin-top: 5rem; text-align:center">
    Products
</h1>
@include('content')



@include('footer')

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
    <script>
        AOS.init();
    </script>
</body>
</html>