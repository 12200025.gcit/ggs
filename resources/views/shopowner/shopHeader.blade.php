@if (auth()->check())
<header>
    <!-- Logo -->
    <a href="/shopHome">
    <img  src="{{ asset('images/logo.png') }}" alt="Logo" width="120" class="logo image-fluid">
    </a>
    <!-- end logo -->
    <nav class="navbar navbar-light navbar-expand-lg  ml-auto">
        <button class="navbar-toggler ms-auto me-3" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon text-dark"></span>
        </button>
      <div class="collapse navbar-collapse navColl" id="navbarNavDropdown">
        <ul class="navbar-nav ml-auto">
          
          <li class="nav-item">
            <a class="nav-link" href="/shop/users">Users </a>
          </li>
          
          
          

          
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="profile-icon">
{{ implode('', array_map(fn($word) => substr($word, 0, 1), explode(' ', auth()->user()->name))) }}
                </div>
                
            </a>
            <div class="dropdown-menu dropdown-menu-right text-center justify-content-center me-4" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item " href="#">
              <div class="d-flex justify-content-center align-items-center">
        <div class="profile-icon-lg">
            {{ implode('', array_map(fn($word) => substr($word, 0, 1), explode(' ', auth()->user()->name))) }}    
        </div>
    
        </div>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    
    
        <div class="profile">
                    <a href="#" style="color:#5C5757;font-weight:500;font-size:14px">
               
                    <p>{{ auth()->user()->name }}</p> 
                    
                    </a>
        </div>
        <div style="border-bottom:1px solid #D9D9D9;margin-left:10px;margin-right:10px"class="pt-2 pb-2">
            <a href="/home/setting" style="color:#5C5757;font-weight:400;font-size:14px">
            <i class="fa fa-cog"></i> Setting
            </a>
        </div>
        <div class="pt-2">
        
        <a href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="color:#5C5757;font-weight:400;font-size:14px">
        <i class="fas fa-sign-out-alt"></i>{{ __('Logout') }}
        </a>
        </div>   
    </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    </header>

    @else
<header>
<a href="/shopHome">
<img  src="{{ asset('images/logo.png') }}" alt="Logo" width="180" class="logo image-fluid">
</a>
<a href="/login" class="login-button mt-4 text-decoration-none text-white rounded d-flex justify-content-center"
>Login</a>
</header>
@endif