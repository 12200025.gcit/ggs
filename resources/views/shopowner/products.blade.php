<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Products</title>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <!-- Include Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/header.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500&display=swap">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <style>
        html {
            overflow: scroll;
            overflow-x: hidden;
        }
        ::-webkit-scrollbar {
            width: 0;  /* Remove scrollbar space */
            background: transparent;  /* Optional: just make scrollbar invisible */
        }
        /* Optional: show position indicator in red */
        ::-webkit-scrollbar-thumb {
            background: #FF0000;
        }
    </style>
</head>
<body style="background-color: #f5f5f5; padding: 0; margin: 0; 
font-family: Arial, sans-serif;">

    @include('shopowner.shopHeader')
    
    <div style="padding: 2rem; margin: 0 20rem 0 20rem; ">

    <h1 style="margin-bottom: 20px;">Add a Product</h1>
    @if(Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif

            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

    <form method="POST" action="{{ route('products.add') }}"  enctype="multipart/form-data" style="margin-bottom: 20px;">
        @csrf
        <label for="name" style="font-weight: bold;">Product Name:</label>
        <input type="text" id="name" name="name" required style="width: 100%; padding: 8px; margin-bottom: 10px; border: 1px solid #ccc; border-radius: 4px;">
        <input type="hidden" id="post_by" name="post_by" value="{{ auth()->id() }}">
        <br>

        <label for="price" style="font-weight: bold;">Price:</label>
        <input type="number" id="price" name="price" required style="width: 100%; padding: 8px; margin-bottom: 10px; border: 1px solid #ccc; border-radius: 4px;"><br>

        <label for="quantity" style="font-weight: bold;">Quantity:</label><br>
        <input type="number" id="quantity" name="quantity" required style="width: 100%; padding: 8px; margin-bottom: 10px; border: 1px solid #ccc; border-radius: 4px;"><br>

        <label for="description" style="font-weight: bold;">Description:</label><br>
        <textarea id="description" name="description" required style="width: 100%; padding: 8px; margin-bottom: 10px; border: 1px solid #ccc; border-radius: 4px;"></textarea><br>

        <label for="image" style="font-weight: bold;">Image:</label>
        <input type="file" id="image" name="image" style="margin-bottom: 20px;"><br>

        <button type="submit" style="padding: 10px 20px; background-color: #4CAF50; color: white; border: none; border-radius: 4px; cursor: pointer;">Add Product</button>
    </form> 
    <h1 style="margin-bottom: 20px;">All Products</h1>
    
    <div class="d-flex flex-wrap justify-content-around" style="overflow-x: auto; margin: 2rem 0;">
        @foreach($products as $product)
        <div style="flex-basis: 20%; margin: 2rem; display: flex; flex-direction: column; align-items: center; text-align: center; background-color:#EAECED; border-radius:0.5rem;">
            <!-- ... Your existing content ... -->
                <img src="{{ asset('images/' . $product->image) }}" style="width: 100%; border-top-left-radius: 0.5rem; border-top-right-radius: 0.5rem;" alt="{{ $product->name }}" />
                <div style="padding: 0.5rem;">{{ $product->name }}</div>
                <div style="padding: 0.5rem;">Price: Nu.{{ $product->price }}</div>
                <div style="padding: 0.5rem;">Quantity available: {{ $product->quantity }}</div>
                <div style="padding: 0.5rem; font-size:16px; color:grey;">{{ $product->description }}</div>

    <form id="removeItem" action="/shop/remove/{{ $product->id }}" method="POST">
        @csrf <!-- Include CSRF token for Laravel -->
        @method('DELETE') <!-- Set the HTTP method to DELETE -->
        
        <!-- Display the Remove button -->
        <button type="submit" class="btn btn-danger btn-sm">Remove</button>
    </form>

            </div>
        @endforeach
    </div>

    </div>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

</body>
</html>
