<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/login.css') }}"> <!-- Link to the external CSS file -->
</head>
<body>
    
    <div style="display: flex; justify-content:center; align-items:center; margin-top: 4rem;">
    <div class="login-container">
        <img src="{{ asset('images/logo.png') }}" alt="GSB logo" style="width:100%; height:auto"> 

<form method="POST" action="{{ route('register') }}">
    @csrf
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    @if(Session::has('error'))    
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    @if(Session::has('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif

    <label>Name</label>
    <input type="text" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>
    <label>Email</label>
    <input type="email" name="email" value="{{ old('email') }}" placeholder="Email" required>
    <label>Password</label>
    <input type="password" name="password" placeholder="Password" required>
    <label>Password Confirm</label>
    <input type="password" name="password_confirmation" placeholder="Password Confirm" required>

    <button type="submit">Register</button>
</form>
<div style="font-size: 12px">Already have an account? <a href="{{ ('login') }}">Login</a></div>

</div>
</div>
</body>
</html>