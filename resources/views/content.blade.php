<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<div class="container" id="prod">
    <div class="d-flex flex-wrap justify-content-around" style="overflow-x: auto; margin: 2rem 0;" data-aos="fade-up">
        @foreach($products as $product)
        <div style="flex-basis: 20%; margin: 2rem; display: flex; flex-direction: column; align-items: center; text-align: center; background-color:#EAECED; border-radius:0.5rem;" data-aos="fade-up">
      <!-- ... Your existing content ... -->
                <img src="{{ asset('images/' . $product->image) }}" style="width: 100%; border-top-left-radius: 0.5rem; border-top-right-radius: 0.5rem;" alt="{{ $product->name }}" />
                <div style="padding: 0.5rem;">{{ $product->name }}</div>
                <div style="padding: 0.5rem;">Price: Nu.{{ $product->price }}</div>
                <div style="padding: 0.5rem;">Quantity available: {{ $product->quantity }}</div>
                <div style="padding: 0.5rem; font-size:16px; color:grey;">{{ $product->description }}</div>
<form id="addToCartForm" action="/cart/add/{{ $product->id }}" method="POST">
    @csrf <!-- Include CSRF token for Laravel -->
    <!-- Hidden input fields for productId and quantity -->
    <input type="hidden" name="productId" id="productIdInput" value="{{ $product->id }}">
    <input type="number" name="quantity" id="quantityInput" value=1 class="form-control-sm rounded-pill text-center" placeholder="Enter quantity">
    <button type="submit" style="margin: 0.5rem; padding: 8px 20px; background-color: #4CAF50; color: white; border: none; border-radius: 4px; cursor: pointer;"
    id="buyNowBtn">Buy now</button>
</form>
</div>
        @endforeach
    </div>
</div>


