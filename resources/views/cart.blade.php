<!-- cart.html -->

<!-- Bootstrap CSS (Replace with the CDN link for the version you are using) -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

<!-- Cart Modal Button -->
<style>
    .cart-button {
        position: fixed;
        bottom: 40px;
        right: 40px;
        width: 60px; /* Set a fixed width */
        height: 60px; /* Set a fixed height */
        border-radius: 50%;
        background-color: #3EC7F4;
        color: #fff;
        border: none;
        cursor: pointer;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .cart-button img {
        max-width: 80%; /* Adjust the image size as needed */
        max-height: 80%; /* Adjust the image size as needed */
    }
   

    @media (max-width: 767px) {
        /* Adjust the button position for small screens */
        .cart-button {
            bottom: 10%;
            right: 10%;
        }
    }
</style>

<button type="button" class="cart-button" data-toggle="modal" data-target="#cartModal" id="openCartModal">
    <img src="{{ asset('images/cart-shopping-solid.svg') }}" alt="Cart Icon">
</button>


<!-- Cart Modal -->
<div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="cartModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="cartModalLabel">Your Shopping Cart</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div style="overflow-x: auto;">
                    <table class="table table-fixed">
                        <thead>
                            <tr>
                                <th scope="col" style="width: 40%;">Product Name</th>
                                <th scope="col" style="width: 20%;">Quantity</th>
                                <th scope="col" style="width: 20%;">Price per Item</th>
                                <th scope="col" style="width: 20%;">Total</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($cartItems) && count($cartItems) > 0)
                            @foreach($cartItems as $cartItem)
                                <tr>
                                    <td>{{ $cartItem->product->name }}</td>
                                    <td>{{ $cartItem->quantity }}</td>
                                    <td>{{ $cartItem->price }}</td>
                                    <td>{{ $cartItem->quantity * $cartItem->price }}</td>
                                    <td>
                                        <form id="removeFromCartForm" action="/cart/remove/{{ $cartItem->product->id }}" method="POST">
                                            @csrf <!-- Include CSRF token for Laravel -->
                                            @method('DELETE') <!-- Set the HTTP method to DELETE -->
                                            
                                            <!-- Display the Remove button -->
                                            <button type="submit" class="btn btn-danger btn-sm">Remove</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">Cart is empty</td>
                            </tr>
                        @endif
                            <!-- Add more cart items as needed -->
                        </tbody>
                        <tfoot>
                            <!-- Total row -->
                            @if(isset($totalPrice))
                            <tr>
                                <td colspan="3" class="text-right"><strong>Total:</strong></td>
                                <td id="cart-total">Nu.{{ number_format($totalPrice, 2) }}</td>
                                <input type="hidden" value={{ number_format($totalPrice, 2) }} name="total_price">
                            </tr>
                            @endif
                        </tfoot>
                    </table>
                </div>
            </div>
            
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <form method="POST" action={{route('checkout')}}>
                    
                    @csrf
                <button type="submit" class="btn btn-primary">Proceed to Checkout</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

<!-- Bootstrap JS and Popper.js (Replace with the CDN links for the versions you are using) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
