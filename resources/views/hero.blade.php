<div style="position: relative;">
    <img src="{{ asset('images/herobanner.jpg') }}" style="width: 100%; height: auto; background-color: white; opacity: 0; animation: fadeInImage 1.5s ease-out forwards;" alt="Hero Banner Image">
    <div style="position: absolute; top: 35%; left: 50%; transform: translate(-50%, -50%); text-align: center; color: #3EC7F4; opacity: 0; animation: fadeInText 1.5s ease-out forwards;">
        <h1 style="font-size: 2vw; max-width: 90vw; background-color: #808080; padding: 10px; border-radius: 5px;">
            Elevating your shopping experience at GCIT
        </h1>
        <a href="#prod" style="display: inline-block; padding: 0.7vw 2vw; background-color: #3EC7F4; color: white; text-decoration: none; border-radius: 5px; margin-top: 5vw; opacity: 0; animation: fadeInButton 1.5s ease-out forwards;">
            Shop now
        </a>
    </div>
</div>

<style>
    @keyframes fadeInImage {
        0% { opacity: 0; transform: translateY(50px); }
        100% { opacity: 1; transform: translateY(0); }
    }

    @keyframes fadeInText {
        0% { opacity: 0; transform: translateX(-50px); }
        100% { opacity: 1; transform: translateX(0); }
    }

    @keyframes fadeInButton {
        0% { opacity: 0; transform: translateY(20px); }
        100% { opacity: 1; transform: translateY(0); }
    }

    /* Media query for heading size and button size */
    @media (min-width: 769px) {
        h1 {
            font-size: 48px;
        }

        a {
            padding: 10px 20px;
            font-size: 16px;
            margin-top: 20px;
        }
    }
</style>
