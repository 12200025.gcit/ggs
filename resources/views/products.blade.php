<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GGS</title>

    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <!-- Include Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/header.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500&display=swap">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <style>
        html {
            overflow: scroll;
            overflow-x: hidden;
        }
        ::-webkit-scrollbar {
            width: 0;  /* Remove scrollbar space */
            background: transparent;  /* Optional: just make scrollbar invisible */
        }
        /* Optional: show position indicator in red */
        ::-webkit-scrollbar-thumb {
            background: #FF0000;
        }

        .searchsort {
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
            }

            .searchsort1{
                margin-bottom: 2rem;
            }
        @media screen and (min-width: 600px) {
            /* Adjust styles for screens larger than 600px wide */
            form {
                flex-direction: row;
                align-items: center;
            }
            .searchsort {
                flex-direction: row;
            }
            .searchsort1{
                margin-bottom: 0;
            }

            input, select {
                margin-bottom: 0;
                margin-right: 6px;
            }
        }
    </style>
</head>

<body>
    
@include('header')
<h2 style="margin-left: 4rem">
    Products
</h2>
@if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{ Session::get('error') }}
                    </div>
                @endif
    
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
<div style="display: flex; justify-content: center; margin-top:2rem;" class="searchsort">
    <form action="{{ route('searchProducts') }}" method="GET" style="display: flex;">
        <input type="text" name="search" placeholder="Search products" style="padding: 8px; border: 1px solid #ccc; border-radius: 4px;" value="{{ request('search') }}">
        <button type="submit" style="padding: 8px 16px; background-color: #4CAF50; color: white; border: none; border-radius: 4px; margin-left: 6px;">Search</button>
    </form>

    <form action="{{ route('sortProducts') }}" method="GET" style="margin-left: 1rem;">
        <label for="sort" style="margin-right: 6px;">Sort by:</label>
        <select name="sort" id="sort" style="padding: 8px; border: 1px solid #ccc; border-radius: 4px;">
            <option value="price_asc">Price (Low to High)</option>
            <option value="price_desc">Price (High to Low)</option>
            <option value="name_asc">Name (A to Z)</option>
            <option value="name_desc">Name (Z to A)</option>
        </select>
        <button type="submit" style="padding: 8px 16px; background-color: #4CAF50; color: white; border: none; border-radius: 4px;">Sort</button>
    </form>
    
</div>
@include('content')
@include('cart')

@include('footer')

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

</body>
</html>